import json
def lambda_handler(event, context):
    return {
        'body': json.dumps([{
            "hash": "string",
            "author": {
                "name": "string",
                "email": "string",
                "date": 0
            }
        }])
    }
