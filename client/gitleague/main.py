from pprint import pprint

import click

from .gen.python.openapi_client import DefaultApi


@click.command()
@click.option("--commit",
              type=click.STRING,
              help='The hash of the commit that will be sent.')
@click.option("--authorName",
              type=click.STRING,
              help='The author who made the commit.')
@click.option("--authorEmail",
              type=click.STRING,
              help='The email of the author.')
@click.option("--authorDate",
              type=click.INT,
              help='The date when the commit was made.')
def main(commit=None, authorname=None, authoremail=None, authordate=None):
    """Light-weight client for interacting with git-league."""

    api = DefaultApi()
    pprint(api.get_commits())


if __name__ == '__main__':
    main()
