#!/bin/bash
which git-league-client
if [ "$?" -ne "0" ]; then
    echo "Could not find git-league-client. Aborting..."
    exit 1
fi

# commit authorName authorEmail date
DIFF_INFO=$(git show --pretty="%H:%an:%ae:%at:")
echo "$DIFF_INFO"
# This will need to change if any of the info we include has a ":"
IFS=":" read -r commit authorName authorEmail authorDate <<< "$DIFF_INFO"
echo "$commit"
echo "$authorName"
echo "$authorEmail"
echo "$authorDate"
unset IFS
git-league-client --commit "$commit" --authorName "$authorName" --authorEmail "$authorName" --authorDate "$authorDate"

